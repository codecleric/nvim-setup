" ABBREVIATIONs don't work if paste is on!
set nopaste
" ABREVIATIONS:
iab hdr# <C-home># -*- coding: utf-8 -*- vim: set ts=2 sw=2 expandtab:
iab ln# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab bk# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Title <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab prop# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Properties <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab main# <C-home>#!/usr/bin/env python<cr><C-end><cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Main <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr><cr>if __name__=='__main__':<cr>pass<cr><bs>

iab imp# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Imports <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab def# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Definitions <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab const# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Constants / Variiables / Etc. <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab pub# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Public Methods <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab pro# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Protected Methods <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iab priv# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>#~ Private Methods <cr>#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

" Shane's special misspellings
iab initalize initialize
iab Initalize Initialize
iab teh the

iab lenght length
iab widht width
iab wiht with

iab slef self
iab sefl self
iab lsef self
iab lesf self
iab esfl self
iab efsl self
iab eslf self

iab pirnt print
iab pritn print
iab reutrn return
iab retunr return
iab retrun return
iab improt import
iab rerp repr
iab rper repr
iab rpre repr

iab uir uri

" Subclass Responsibility template
iab SCR NotImplementedError('Subclass Responsibility: %r' % (self,))
iab TODOR NotImplementedError('TODO: %r' % (self,))

iab style# <style type="text/css"><cr></style>kA
iab script# <script type="text/javascript"><cr></script>kA
iab js# <script type="text/javascript" src=""> </script>11hi
iab css# <link type="text/css" rel="stylesheet" href="" />4hi
iab link# <link type="text/css" rel="stylesheet" href="" />4hi

:iabbrev ssig -- <cr>Brian Brown<cr>brian@codecleric.com
